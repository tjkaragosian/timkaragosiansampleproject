https://gitlab.com/tjkaragosian/timkaragosiansampleproject/tree/master/app/src/main/java/timkaragosian/timkaragosiansampleproject
https://gitlab.com/tjkaragosian/timkaragosiansampleproject/commit/f1daa40485909e5487cb3dfbf31653ca32875cb3

The purpose of this code is to show my skills in a sample android project.
This project will allow users to log in with an email address and password
and users can sign up through the app. The next Activity after the LogInActivity
will allow the user to navigate to 3 different Fragments using the tabs at the top.
I have disabled the sliding of the tabs. These tabs will allow the user to add
a string to a table in a remote database, view the strings the user has added,
and log out of the app. The data is transferred securely with an HMAC and the 
app uses APIs with SSL certificates. 

The APIs may or may not work with the app as I am still in the process of learning PHP for
Web API use and may not have been able to test all the features as a result.

9/2/16 Application allows registering and logging in. Successfully creates JSON but API needs developing first.

9/6/16 Everything is fully functioning. I will have to develop some features in the future for more tabs.