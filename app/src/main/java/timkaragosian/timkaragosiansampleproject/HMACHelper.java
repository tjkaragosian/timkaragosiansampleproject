package timkaragosian.timkaragosiansampleproject;

import android.util.Base64;
import android.util.Log;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by TJ on 8/31/2016.
 */
public class HMACHelper {
    static public String GenerateHMAC (String email, String currentTimestamp, String url, String apiKey){
        String generatedHMAC = null;
        generatedHMAC = (base64sha256("email\n"+ email +"\ntime\n" + currentTimestamp + "\nurl\n" + url + "\n", apiKey));
        Log.d("hmac_generated", generatedHMAC);
        return generatedHMAC;
    }

    public static String base64sha256(String data, String secret) {
        try {
            Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);
            byte[] res = sha256_HMAC.doFinal(data.getBytes());
            return Base64.encodeToString(res, Base64.NO_WRAP);
        } catch (Exception e) {
        } return null;
    }
}
