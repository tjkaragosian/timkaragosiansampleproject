package timkaragosian.timkaragosiansampleproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by TJ on 8/31/2016.
 */
public class TabViewPagerAdapter extends FragmentPagerAdapter{
    private ArrayList<Fragment> fragmentList = new ArrayList<>();

    public TabViewPagerAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return (fragmentList.size());
    }

    public void addFragment(Fragment fragment){
        fragmentList.add(fragment);
    }
}
