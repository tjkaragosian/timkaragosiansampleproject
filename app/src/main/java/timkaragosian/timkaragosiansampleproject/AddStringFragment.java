package timkaragosian.timkaragosiansampleproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddStringFragment extends Fragment {

    Button addStringSubmitButton;
    EditText addStringEditText;

    public AddStringFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_strings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addStringSubmitButton = (Button) getView().findViewById(R.id.AddStringSubmitButton);
        addStringEditText = (EditText) getView().findViewById(R.id.AddStringEditText);

        addStringSubmitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ((MainNavigationActivity)getActivity()).AddString(addStringEditText.getText().toString());
            }
        });

        addStringEditText.setText("");
    }
}
