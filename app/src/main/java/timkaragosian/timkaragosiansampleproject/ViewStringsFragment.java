package timkaragosian.timkaragosiansampleproject;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewStringsFragment extends Fragment {

    String[] stringsToDisplay;

    Button getStringsButton;


    public ViewStringsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_strings, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getStringsButton = (Button) getView().findViewById(R.id.GetStringsButton);

        getStringsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                stringsToDisplay = ((MainNavigationActivity)getActivity()).ViewStrings();
                ListView stringsListView = (ListView) getView().findViewById(R.id.ViewStringsListView);

                stringsListView.setAdapter(null);

                ArrayAdapter stringsArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, stringsToDisplay);
                stringsListView.setAdapter(stringsArrayAdapter);
            }
        });

    }
}
