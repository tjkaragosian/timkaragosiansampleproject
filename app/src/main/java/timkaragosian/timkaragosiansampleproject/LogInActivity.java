package timkaragosian.timkaragosiansampleproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class LogInActivity extends AppCompatActivity {

    public CheckBox rememberMeCheckBox;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLoginBool;
    public String username, password, apiKey;

    public EditText passwordEditText;
    public Button loginButton;
    public EditText emailEditText;

    private String readStream(InputStream inputStream) throws IOException { //reads lines of code sent back and inputs them into a json object
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        inputStream.close();
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        emailEditText = (EditText) findViewById(R.id.EmailEditText);
        passwordEditText = (EditText) findViewById(R.id.PasswordEditText);
        final TextView registerLink = (TextView) findViewById(R.id.RegisterLinkTextView);
        loginButton = (Button)findViewById(R.id.LoginButton);
        rememberMeCheckBox = (CheckBox) findViewById(R.id.rememberMeCheckBox);
        loginPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loginPrefsEditor = loginPreferences.edit();
        saveLoginBool = loginPreferences.getBoolean("saveLoginBool", false);

        if (saveLoginBool == true) {
            emailEditText.setText(loginPreferences.getString("username", ""));
            passwordEditText.setText(loginPreferences.getString("password", ""));
            apiKey = loginPreferences.getString("apikey", "");
            rememberMeCheckBox.setChecked(true);
        }

        registerLink.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent registerIntent = new Intent(LogInActivity.this, SignUpActivity.class);
                LogInActivity.this.startActivity(registerIntent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(emailEditText.getWindowToken(), 0); //hides the user softkey (keyboard)

                username = emailEditText.getText().toString(); // sets email entered to string username
                password = passwordEditText.getText().toString(); //sets password entered to string password

                loginButton.setEnabled(false);

                Thread loginThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Boolean displayErrorMessage = true;
                        /* 5 lines */
                        if (HandleAuth(username, password)) {
                            String apiKey = HandleGetAPIKey(emailEditText.getText().toString(), passwordEditText.getText().toString());
                            if (apiKey.length() > 0) {
                                displayErrorMessage = false;
                                SavePreferences(username, apiKey);
                                LaunchNextActivity();
                            }
                        }

                        if (displayErrorMessage) {
                            new Thread() {
                                public void run() {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            loginButton.setEnabled(true);
                                        }
                                    });
                                }
                            }.start();
                        }
                    }
                });
                loginThread.start();
            }
        });

        if (loginPreferences.getString("apikey","").length()>0)
        {
            username = loginPreferences.getString("username", username);
            apiKey = loginPreferences.getString("apikey", apiKey);
            LaunchNextActivity();
        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public boolean HandleAuth(String email, String password) {
        String jsonString = null;
        URL url = null;
        HttpURLConnection urlConnection = null;
        email = email.trim();

        try {
            String encodedEmail;
            String encodedPassword;

            try {
                encodedEmail = URLEncoder.encode(email, "UTF-8");
                encodedPassword = URLEncoder.encode(password, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }

            url = new URL("https://example.tkbil.com/api/1/authenticate.php?email=" +
                    encodedEmail + "&password=" + encodedPassword);

            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = null;
            in = new BufferedInputStream(urlConnection.getInputStream());
            jsonString = readStream(in);

            JSONObject jsonObject = new JSONObject(jsonString);

            if (jsonObject.has("error")) {
                Log.d("sign in error", "Error: input stream");
                new Thread() {
                    public void run() {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                }
                            });
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
                return false;
            }

            if (jsonObject.has("result")) {
                if (jsonObject.getBoolean("result") == true) {
                    return true;
                } else if (jsonObject.getBoolean("result") == false) {
                    return false;
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: URL");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: IO");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: JSON");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return false;
    }

    public String HandleGetAPIKey(String email, String password){
        String jsonString = null;
        URL url = null;
        HttpURLConnection urlConnection = null;
        try {
            String encodedEmail;
            String encodedPassword;

            try {
                encodedEmail = URLEncoder.encode(email, "UTF-8");
                encodedPassword = URLEncoder.encode(password, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }

            url = new URL("https://example.tkbil.com/api/1/get_api_key.php?email=" +
                    encodedEmail + "&password=" + encodedPassword);
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = null;
            in = new BufferedInputStream(urlConnection.getInputStream());
            jsonString = readStream(in);

            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.has("error")) {
                Log.d("sign in error", "Error: JSON");
                new Thread() {
                    public void run() {
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                }
                            });
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
                return "";
            }
            if (jsonObject.has("result")) {
                return jsonObject.getString("result");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: URL");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: IO");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return "";
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: JSON");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return "";
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return "";
    }

    public void SavePreferences(String email, String apiKey){

        loginPrefsEditor.putString("currentapikey", apiKey);
        loginPrefsEditor.putString("currentemail", email.trim());

        if (rememberMeCheckBox.isChecked()) {
            loginPrefsEditor.putBoolean("saveLoginBool", true);
            loginPrefsEditor.putString("email", email.trim());
            loginPrefsEditor.putString("apikey", apiKey);
            loginPrefsEditor.commit();
        } else {
            loginPrefsEditor.clear();
            loginPrefsEditor.commit();
        }
    }

    public void LaunchNextActivity(){
        Intent mainNavigationActivityIntent = new Intent(getApplicationContext(), MainNavigationActivity.class);
        mainNavigationActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainNavigationActivityIntent);
        finish();
    }
}
