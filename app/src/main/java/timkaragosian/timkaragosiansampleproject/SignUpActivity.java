package timkaragosian.timkaragosiansampleproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class SignUpActivity extends AppCompatActivity {

    private String readStream(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        inputStream.close();
        return sb.toString();
    }

    public void successfulRegisterToast(){
        Toast.makeText(this, "Sign Up Successful!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        final EditText emailEditText = (EditText) findViewById(R.id.EmailEditText);
        final EditText passwordEditText = (EditText) findViewById(R.id.PasswordEditText);

        final Button signUpButton = (Button)findViewById(R.id.SignUpSubmitButton);

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signUpButton.setEnabled(false);

                Thread registerThread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        String jsonString = null;
                        URL url = null;
                        HttpURLConnection urlConnection = null;

                        try {

                            String encodedEmail;
                            String encodedPassword;

                            try {

                                encodedEmail = URLEncoder.encode(emailEditText.getText().toString().trim(), "UTF-8");
                                encodedPassword = URLEncoder.encode(passwordEditText.getText().toString(), "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                return;
                            }

                            url = new URL("https://example.tkbil.com/api/1/user_create.php?email=" +
                                    encodedEmail + "&password=" + encodedPassword);

                            urlConnection = (HttpURLConnection) url.openConnection();
                            InputStream in = null;
                            in = new BufferedInputStream(urlConnection.getInputStream());
                            jsonString = readStream(in);
                            Log.d("JSON STRING", jsonString);

                            JSONObject jsonObject = new JSONObject(jsonString);

                            if (jsonObject.has("error")) {
                                Log.d("sign in error", "Error: input stream");
                                new Thread() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Error Signing Up. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                            Thread.sleep(300);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.start();
                                return;
                            }
                            if (jsonObject.has("result")) {
                                new Thread() {
                                    public void run() {
                                        try {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Intent loginIntent = new Intent(SignUpActivity.this, LogInActivity.class);
                                                    loginIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                    startActivity(loginIntent);
                                                    successfulRegisterToast();
                                                    finish();
                                                }
                                            });
                                            Thread.sleep(300);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }.start();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            Log.d("sign up error", "Error: URL");
                            new Thread() {
                                public void run() {
                                    try {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Error Signing Up. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.d("sign up error", "Error: IO");
                            new Thread() {
                                public void run() {
                                    try {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Error Signing Up. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("sign up error", "Error: JSON");
                            new Thread() {
                                public void run() {
                                    try {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(getApplicationContext(), "Error Signing In. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();                        } finally {
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                            new Thread() {
                                public void run() {
                                    try {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                signUpButton.setEnabled(true);
                                            }
                                        });
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                    }
                });
                registerThread.start();
            }

        });
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
