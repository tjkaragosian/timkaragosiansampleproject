package timkaragosian.timkaragosiansampleproject;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by TJ on 8/31/2016.
 */
public class DisableSlidingCustomViewPager extends android.support.v4.view.ViewPager {
    private boolean enabledSliding;

    public DisableSlidingCustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabledSliding=true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return enabledSliding ? super.onTouchEvent(event) : false;
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        return enabledSliding ? super.onInterceptHoverEvent(event) : false;
    }

    public void setPagingEnabled(boolean enabled){
        this.enabledSliding = enabled;
    }

    public boolean isPagingEnabled(){
        return enabledSliding;
    }
}
