package timkaragosian.timkaragosiansampleproject;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class MainNavigationActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private DisableSlidingCustomViewPager tabViewPager;

    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;

    public String username, apiKey;

    public JSONArray arrayOfStrings;

    List<String> stringsToSend;

    private String readStream(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(inputStream),1000);
        for (String line = r.readLine(); line != null; line =r.readLine()){
            sb.append(line);
        }
        inputStream.close();
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);

        SharedPreferences userPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        username = userPreferences.getString("currentemail","");
        apiKey = userPreferences.getString("currentapikey","");

        tabViewPager = (DisableSlidingCustomViewPager) findViewById(R.id.tabviewpager);
        tabViewPager.setPagingEnabled(false); //turns off sliding tabs
        setupViewPager(tabViewPager);
        tabViewPager.setCurrentItem(0);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        final TabLayout.Tab add_string=tabLayout.newTab();
        final TabLayout.Tab view_strings=tabLayout.newTab();
        final TabLayout.Tab log_out=tabLayout.newTab();

        add_string.setText("Get Outfit");
        view_strings.setText("My Closet");
        log_out.setText("Saved Outfits");

        View addStringView = getLayoutInflater().inflate(R.layout.custom_tab_view,null);
        ImageView addStringImageView=(ImageView) addStringView.findViewById(R.id.tab_imageview);
        TextView addStringTextView = (TextView) addStringView.findViewById(R.id.tab_textview);
        addStringImageView.setImageResource(R.drawable.ic_plus);
        addStringTextView.setText("Add String");

        View viewStringsView = getLayoutInflater().inflate(R.layout.custom_tab_view,null);
        ImageView viewStringsImageView=(ImageView) viewStringsView.findViewById(R.id.tab_imageview);
        TextView viewStringsTextView = (TextView) viewStringsView.findViewById(R.id.tab_textview);
        viewStringsImageView.setImageResource(R.drawable.ic_down_arrow);
        viewStringsTextView.setText("View Strings");

        View logOutView = getLayoutInflater().inflate(R.layout.custom_tab_view,null);
        ImageView logOutImageView=(ImageView) logOutView.findViewById(R.id.tab_imageview);
        TextView logOutTextView = (TextView) logOutView.findViewById(R.id.tab_textview);
        logOutImageView.setImageResource(R.drawable.ic_power);
        logOutTextView.setText("Log Out");

        add_string.setCustomView(addStringView);
        view_strings.setCustomView(viewStringsView);
        log_out.setCustomView(logOutView);

        tabLayout.addTab(add_string,0);
        tabLayout.addTab(view_strings,1);
        tabLayout.addTab(log_out,2);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                    tabViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private void setupViewPager(ViewPager viewPager){

        AddStringFragment addStringFragment = new AddStringFragment();
        ViewStringsFragment viewStringsFragment = new ViewStringsFragment();
        LogOutFragment logOutFragment = new LogOutFragment();

        TabViewPagerAdapter tabViewPagerAdapter = new TabViewPagerAdapter(getSupportFragmentManager());
        tabViewPagerAdapter.addFragment(addStringFragment);
        tabViewPagerAdapter.addFragment(viewStringsFragment);
        tabViewPagerAdapter.addFragment(logOutFragment);
        viewPager.setAdapter(tabViewPagerAdapter);
    }

    public void LogOutAction(){
        Intent loginIntent = new Intent(MainNavigationActivity.this, LogInActivity.class);
        loginPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        loginPrefsEditor = loginPreferences.edit();
        loginPrefsEditor.clear().commit();
        startActivity(loginIntent);
        Toast.makeText(this, "Logged Out Successfully",Toast.LENGTH_SHORT).show();
        finish();
    }

    public void AddString(final String stringToAdd){
        //code to add string to database
        Thread addStringThread = new Thread(new Runnable() {
            @Override
            public void run() {
                if(HandleAddStringToDatabase(username, stringToAdd)){
                    new Thread() {
                        public void run() {
                            try {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(getApplicationContext(),"You just added the string " + stringToAdd + "!", Toast.LENGTH_SHORT).show();
                                    }
                                });
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
            }
        });
        addStringThread.start();
    }

    public String[] ViewStrings(){

        stringsToSend = new ArrayList<String>();

        getStringsHandler();

        String[] stringsValues = stringsToSend.toArray(new String[stringsToSend.size()]);

        return stringsValues;
    }

    public void getStringsHandler(){
        Thread getStringsThread = new Thread(new Runnable() {
            @Override
            public void run() {
                HandleGetStrings(username, apiKey);
            }
        });
        getStringsThread.start();
        try {
            getStringsThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean HandleGetStrings(String email, String apiKey) {
        String jsonString = null;
        URL url = null;
        HttpURLConnection urlConnection = null;

        try {
            Long tempTimestamp = System.currentTimeMillis()/1000;
            String currentTimestamp = tempTimestamp.toString();
            String encodedEmail;
            String encodedHMAC = HMACHelper.GenerateHMAC(username.trim(), currentTimestamp,
                    "/api/1/get_strings.php", apiKey);
            try {
                encodedEmail = URLEncoder.encode(email, "UTF-8");
                encodedHMAC = URLEncoder.encode(encodedHMAC, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }
            url = new URL("https://example.tkbil.com/api/1/get_strings.php?email=" + encodedEmail +
                    "&time=" + currentTimestamp + "&hmac=" + encodedHMAC);

            Log.d("url_sent", url.toString());
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = null;
            in = new BufferedInputStream(urlConnection.getInputStream());
            jsonString = readStream(in);
            Log.d("URL Response", jsonString.toString());
            JSONObject jsonObject = new JSONObject(jsonString);

            if (jsonObject.has("error")) {
                return false;
            }

            if (jsonObject.has("result")) {
                arrayOfStrings = jsonObject.getJSONArray("result");
                for (int i =0; i<arrayOfStrings.length(); i++) {
                    JSONArray stringJsonArray = arrayOfStrings.getJSONArray(i);
                    stringsToSend.add(stringJsonArray.getString(0));
                }
                return true;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
            //Toast.makeText(getApplicationContext(), "Error: URL not found", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            return false;

            //Toast.makeText(getApplicationContext(), "Error: error logging in", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            e.printStackTrace();
            return false;

            //Toast.makeText(getApplicationContext(), "Error: error logging in", Toast.LENGTH_SHORT).show();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return false;
    }

    public boolean HandleAddStringToDatabase(String email, String stringToAdd) {
        String jsonString = null;
        URL url = null;
        HttpURLConnection urlConnection = null;

        Log.d("API key", apiKey.toString() + " " + username.trim());

        if (stringToAdd==null)
        {
            return false;
        }


        try {
            Long tempTimestamp = System.currentTimeMillis()/1000;
            String currentTimestamp = tempTimestamp.toString();
            String encodedEmail;
            String encodedHMAC = HMACHelper.GenerateHMAC(username.trim(), currentTimestamp,
                    "/api/1/add_string.php", apiKey);
            JSONArray jsonStringToAdd = new JSONArray();
            String encodedStringToAdd = URLEncoder.encode(stringToAdd,"UTF-8");
            jsonStringToAdd.put(encodedStringToAdd);

            Log.d("API key", apiKey.toString() + " " + username.trim());

            try {
                encodedEmail = URLEncoder.encode(email, "UTF-8");
                encodedHMAC = URLEncoder.encode(encodedHMAC, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return false;
            }

            url = new URL("https://example.tkbil.com/api/1/add_string.php?email=" + encodedEmail +
                    "&time=" + currentTimestamp + "&hmac=" + encodedHMAC + "&clothing=" + jsonStringToAdd);

            Log.d("url_sent", url.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = null;
            in = new BufferedInputStream(urlConnection.getInputStream());
            jsonString = readStream(in);

            Log.d("URL Response", jsonString.toString());

            JSONObject jsonObject = new JSONObject(jsonString);

            if (jsonObject.has("error")) {
                return false;
            }

            if (jsonObject.has("result")) {
                return true;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.d("add string error", "Error: URL");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error adding string. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("sign in error", "Error: IO");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error adding string. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("add string error", "Error: JSON");
            new Thread() {
                public void run() {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Error adding string. Please contact timkaragosian@gmail.com", Toast.LENGTH_SHORT).show();
                            }
                        });
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.start();
            return false;
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return false;
    }
}
